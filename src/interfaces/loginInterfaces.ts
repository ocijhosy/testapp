export interface user{
    name: string;
    email:string;
    password:string;
    date:string;
}

export interface loginResponse{
    success: boolean;
    message: string;
}

export interface userRegistrationResponse{
    success: boolean;
    message: string;
}

  export interface userInfoAPIResponse{
    gender: string;
    name: {
      title: string;
      first: string;
      last: string;
    };
    email: string;
    login: {
      username: string;
      password: string;
    };
    registered: {
      date: string;
    };
  }

  export interface apiCallResponse{
   results?: userInfoAPIResponse[];     
  }

  