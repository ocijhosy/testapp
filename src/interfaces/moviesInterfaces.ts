export interface moviesApiCallResponse{
    movies: movie[];
}

export interface movie{
    show?: show;
    score?: number;
}

export interface show {
    id: number;
    url: string;
    name: string;
    type: string;
    language: string;
    genres: string[];
    status: string;
    runtime: number | null;
    averageRuntime: number;
    premiered: string;
    ended: string | null;
    officialSite: string | null;
    schedule: {
      time: string;
      days: string[];
    };
    rating: {
      average: number | null;
    };
    weight: number;
    network: {
      id: number;
      name: string;
      country: {
        name: string;
        code: string;
        timezone: string;
      };
      officialSite: string | null;
    } | null;
    webChannel: {
      id: number;
      name: string;
      country: {
        name: string;
        code: string;
        timezone: string;
      } | null;
      officialSite: string | null;
    } | null;
    dvdCountry: any; // Define el tipo adecuado para esta propiedad si se conoce
    externals: {
      tvrage: number | null;
      thetvdb: number;
      imdb: string | null;
    };
    image: {
      medium: string;
      original: string;
    } | null;
    summary: string;
    updated: number;
    _links: {
      self: {
        href: string;
      };
      previousepisode: {
        href: string;
        name: string;
      } | null;
      nextepisode?: {
        href: string;
        name: string;
      } | null;
    };
  }