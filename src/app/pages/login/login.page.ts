import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { user } from 'src/interfaces/loginInterfaces';
import { UserServicesService } from 'src/app/services/user-services.service';
import { formatDate } from '@angular/common';



@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit  {

  credentials: FormGroup;
  registrationInfo: FormGroup;
  loginScreen=true;

  constructor(
    private fb: FormBuilder,
    // private authService: AuthenticationService,
    private alertController: AlertController,
    private router: Router,
    private loadingController: LoadingController,
    private userService: UserServicesService,
    private storageService: LocalStorageService
  ) {
    this.credentials= this.fb.group({
      email: ['',[Validators.required, Validators.email]],
      password:['',[Validators.required, Validators.minLength(1)]]
    });
    this.registrationInfo= this.fb.group({
      name: ['',[Validators.required]],
      email: ['ociel.castelan.m@vde.com.mx',[Validators.required, Validators.email]],
      password:['Access1234!',[Validators.required, Validators.minLength(1)]]
    });
  }

  async ngOnInit():Promise<void> {
    // this.storageService.read('client');
    await this.userService.getUserInfo().then(respFromService=>{
      let apiName= respFromService.name.title+" "+respFromService.name.first+" "+respFromService.name.last;
      this.registrationInfo.setValue({
        name:apiName,
        email:respFromService.email,
        password:respFromService.login.password
      });
    });
  }

  async login(): Promise<void>{
   await this.userService.login(this.credentials.get('email')?.value,this.credentials.get('password')?.value)
   .then(async respService=>{
        const alert =  await this.alertController.create({
          message: respService.message,
          buttons: ['OK'],
          mode: 'ios'
        });
        await alert.present();
        await alert.onDidDismiss().then(resp=>{
          if(respService.success){
            this.router.navigateByUrl('/inicio',{});
          }
        });
   });
  }

  changeView(): void{
    this.loginScreen=!this.loginScreen;
  }

  async registerNewUser(): Promise<void>{
        let fecha = formatDate(new Date(), 'dd-MM-yyyy', 'en-US')
        let newUser: user={
          name:this.registrationInfo.get('name')?.value,
          email:this.registrationInfo.get('email')?.value,
          password:this.registrationInfo.get('password')?.value,
          date: fecha
        };
        let alertContent=`\
        Registrando usuario con los siguientes datos<br><br><br>
        <b>Nombre:</b><br> ${newUser.name}<br><br>
        <b>email:</b><br> ${newUser.email}<br><br>
        <b>password:</b><br> ${newUser.password}<br><br>
        <b>Fecha de Registro:</b><br> ${newUser.date}<br><br>
        `;
        const alert =  await this.alertController.create({
          message: alertContent,
          buttons: ['OK'],
          mode: 'ios'
        });
        await alert.present();
        await alert.onDidDismiss().then(async resp=>{
          await this.userService.registerNewUser(newUser).then(async respService=>{
            if(respService.success){
              alertContent=`\ Usuario registrado con exito, ahora puede iniciar sesion.`;
              this.credentials.setValue({
                email:newUser.email,
                password:newUser.password
              });
              this.changeView();
            }
            else{
              alertContent=respService.message;
            }
            const alertResponse =  await this.alertController.create({
              message: alertContent,
              buttons: ['OK'],
              mode: 'ios'
            });
          await alertResponse.present();
          });
        });

        
  }
  

  get email(){
    return this.credentials.get('email');
  }

  get password(){
    return this.credentials.get('password');
  }

  get registrationEmail(){
    return this.registrationInfo.get('email');
  }

  get registrationPassword(){
    return this.registrationInfo.get('password');
  }

  get registrationName(){
    return this.registrationInfo.get('name');
  }

}