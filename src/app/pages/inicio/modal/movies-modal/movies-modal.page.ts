import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { movie } from 'src/interfaces/moviesInterfaces';

@Component({
  selector: 'app-movies-modal',
  templateUrl: './movies-modal.page.html',
  styleUrls: ['./movies-modal.page.scss'],
})
export class MoviesModalPage implements OnInit {

  @Input() movie: movie={};


  constructor(
    private modalCtrl: ModalController,
    
  ) { }

  ngOnInit() {

  }

  cancel():void {
    this.modalCtrl.dismiss({});
  }

}
