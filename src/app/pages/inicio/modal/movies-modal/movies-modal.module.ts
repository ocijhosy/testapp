import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MoviesModalPageRoutingModule } from './movies-modal-routing.module';

import { MoviesModalPage } from './movies-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MoviesModalPageRoutingModule
  ],
  declarations: [MoviesModalPage]
})
export class MoviesModalPageModule {}
