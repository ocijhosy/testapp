import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MoviesModalPage } from './movies-modal.page';

describe('MoviesModalPage', () => {
  let component: MoviesModalPage;
  let fixture: ComponentFixture<MoviesModalPage>;

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviesModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
