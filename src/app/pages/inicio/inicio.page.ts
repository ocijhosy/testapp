import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { MovieServiceService } from 'src/app/services/movie-service.service';
import { movie } from 'src/interfaces/moviesInterfaces';
import { MoviesModalPage } from './modal/movies-modal/movies-modal.page';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  constructor(
    private moviesService: MovieServiceService,
    private modalCtrl: ModalController,
  ) { }

  movies: movie[]=[];
  filter=""

  async ngOnInit() {
    await this.moviesService.getMovies().then(respService=>{
      console.log(respService);
      this.movies=respService.movies;
    });
    this.movies.forEach(element=>{
      if(element!.show!.image&& element!.show!.image.medium){

      }
      else{
        console.log('not found');
        element!.show!.image={original:'',medium:'https://upload.wikimedia.org/wikipedia/commons/a/a3/Image-not-found.png'};
      }
    })
  }

  updateFilter(event:any):void{
  this.filter=event.detail.value;

  }

  async showModal(movie: movie){
    const modal = await this.modalCtrl.create({
      component: MoviesModalPage,
      componentProps: {
          movie
      },
  });
  await modal.present();
  }

}
