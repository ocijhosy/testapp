import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { movie, moviesApiCallResponse } from 'src/interfaces/moviesInterfaces';


@Injectable({
  providedIn: 'root'
})
export class MovieServiceService {

  constructor(
    private http: HttpClient,

  ) { }

  getMovies(){
    return new Promise<moviesApiCallResponse>(async resolve => {
      this.http.get<moviesApiCallResponse>('https://api.tvmaze.com/search/shows?q=drama',{})
        .subscribe(async (responseFromAPI :any) => {
          console.log(responseFromAPI);
          let response: moviesApiCallResponse={
            movies:[]
          }
          responseFromAPI.forEach((element:movie) => {
            response.movies.push(element);
          });
          resolve(response);
        });
    });
  }
}
