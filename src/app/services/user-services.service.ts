import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { apiCallResponse, loginResponse, user, userInfoAPIResponse } from 'src/interfaces/loginInterfaces';
import { userRegistrationResponse } from '../../interfaces/loginInterfaces';
import { LocalStorageService } from './local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class UserServicesService {

  constructor(
    private http: HttpClient,
    private localStorageService:LocalStorageService

  ) { }


  async login(email: string, password: string): Promise<loginResponse>{
    let success=false;
    let users: user[]=[];
    let message='Usuario o contraseña incorrectos';

    await this.localStorageService.read('users').then(respService=>{
      if(respService!==null){
        JSON.parse(respService).forEach((element: user) => {
          if(element.email===email && element.password===password){
            success=true;
            message='Informacion correcta, Bienvenido!'
          }
      });
      }
    });

    return {success: success, message:message};
  }

  async registerNewUser(user: user):Promise<userRegistrationResponse>{
    let success=true;
    let users: user[]=[];
    let message='';

    await this.localStorageService.read('users').then(respService=>{
      if(respService!==null){
        JSON.parse(respService).forEach((element: user) => {
          if(element.email===user.email){
            success=false;
            message='email ya registrado, intente con uno nuevo o iniciar sesion'
          }
          else{
          users.push(element);
          }
      });
      }
      
    });

    if(success){
      users.push(user);
    }

    await this.localStorageService.write('users',JSON.stringify(users));
    
    return {success: success, message:message};
  }



  async getUserInfo(): Promise<userInfoAPIResponse>{

    return new Promise<userInfoAPIResponse>(async resolve => {
      this.http.get<userInfoAPIResponse>('https://randomuser.me/api',{})
        .subscribe(async responseFromAPI => {
          let response =responseFromAPI as apiCallResponse;
          if(response.results && response.results.length > 0){
          resolve(response.results[0]);
          }
        });
    });
  }
}
