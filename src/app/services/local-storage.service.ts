import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';


@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor(
    private localStorage: Storage 
  ) { 
    localStorage.create();
  }

  async read(key:string): Promise<string>{
    let response='';
    await this.localStorage.get(key).then(resp=>{
      response=resp;
    });
    return response;
  }

  async write(key:string, data:string){
    await this.localStorage.set(key,data);
  }

}
